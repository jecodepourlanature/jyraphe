<?php
/*header("Vary: Accept");
if(stristr($_SERVER['HTTP_ACCEPT'], 'application/xhtml+xml')) {
  $content_type = 'application/xhtml+xml; charset=utf-8';
}  else {
  $content_type = 'text/html; charset=utf-8';
}*/
  $content_type = 'text/html; charset=utf-8';

header('Content-Type: ' . $content_type);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?php echo _('Jyraphe, your web file repository'); ?></title>
  <meta http-equiv="Content-Type" content="<?php echo $content_type; ?>" />
  <link href="<?php echo $cfg['web_root'] . 'media/' . $cfg['style'] . '/style.css.php'; ?>" rel="stylesheet" type="text/css" />
</head>
<body>

<div id="bandeau">
  <span class="home">
    <a href="http://www.picardie-nature.org">Picardie Nature</a>
  </span>
  <span class="links">  
    <a href="http://adherents.picardie-nature.org">Espace adhérent</a> ·
    <a href="http://dl.picardie-nature.org">File Sharing</a> ·
    <a href="http://webmail.picardie-nature.org">Webmail</a>
  </span>
</div>

<div id="content">
<h1><a href="<?php echo $cfg['web_root']; ?>"><?php echo _('Jyraphe, your web file repository'); ?></a></h1>
 <h5>Cet espace vous permet de diffuser simplement des documents. Envoyez votre fichier, choisissez éventuellement un mot de passe et la durée de vie sur le dépôt et cliquez sur Envoyer. En retour, vous obtiendez un lien internet à copier coller dans votre mail. En cas de problème, contactez le G.T.I.</h5>
